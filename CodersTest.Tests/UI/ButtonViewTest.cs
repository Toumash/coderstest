﻿using CodersTest.Views;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodersTest.Tests.UI
{
    [TestClass]
    public class ButtonViewTest
    {
        [TestMethod]
        public void should_be_clickable()
        {
            var sut = new ButtonView(ConsoleConfig.Default);
            var thisVariableShouldChange = 0;
            sut.Click(() => thisVariableShouldChange = 1);

            // sending actual clck event
            sut.Click();

            var buttonClicked = thisVariableShouldChange != 0;
            Assert.IsTrue(buttonClicked);
        }
    }
}
