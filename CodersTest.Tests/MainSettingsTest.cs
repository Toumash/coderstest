﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodersTest;

namespace CodersTest.Tests
{
    [TestClass]
    public class MainSettingsTest
    {
        [TestMethod]
        public void default_background_should_be_darkcyan()
        {
            var sut = ConsoleConfig.Default;
            Assert.AreEqual(sut.BackgroundColor, ConsoleColor.DarkCyan);
        }

        [TestMethod]
        public void default_checkbox_should_be_red()
        {
            var sut = ConsoleConfig.Default;
            Assert.AreEqual(sut.NotSelectedColor, ConsoleColor.DarkRed);
        }

        [TestMethod]
        public void colors_shouldnt_be_gay()
        {
            var sut = ConsoleConfig.Default;
            var reflection = sut.GetType();
            var properties = reflection.GetProperties();
            foreach (var prop in properties)
            {
                var value = prop.GetValue(sut);
                if (value is ConsoleColor)
                {
                    Assert.AreNotEqual(value, ConsoleColor.Magenta, $"Property:{prop.Name} is gay color");
                }
            }
        }
    }
}
