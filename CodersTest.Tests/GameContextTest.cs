﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersTest.Tests
{
    [TestClass]
    public class GameContextTest
    {
        [TestMethod]
        public void should_use_default_configuration_at_startup()
        {
            var sut = new GameContext();

            Assert.AreEqual(sut.Config, ConsoleConfig.Default);
        }
    }
}
