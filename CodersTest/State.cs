﻿namespace CodersTest
{
    public abstract class GameState
    {
        protected GameContext Context;

        protected GameState(GameContext context)
        {
            this.Context = context;
        }

        abstract public void GoNext();
        public virtual void OnEnterState() { }
        public virtual void OnExitState() { }
    }
}
