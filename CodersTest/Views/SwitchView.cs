﻿using CodersTest.Views;
using System;

namespace CodersTest.Views
{
    public class SwitchView : View, IClickable
    {
        private bool SwitchedOn;
        private string text = string.Empty;
        private Action<bool> onClick;

        public SwitchView(ConsoleConfig config) : base(0, 0, config)
        {
            SwitchedOn = false;
        }

        public void Click()
        {
            SwitchedOn = !SwitchedOn;
            onClick?.Invoke(SwitchedOn);
        }

        public SwitchView SetOnClick(Action<bool> action)
        {
            onClick = action;
            return this;
        }

        public override void Render(bool selected = false)
        {
            var bgColor = Console.BackgroundColor;
            var fgColor = Console.ForegroundColor;

            if (selected)
            {
                Console.BackgroundColor = Config.BackgroundColor;
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.Write(this.text);
            Console.Write('\t');

            if (SwitchedOn)
            {
                Console.ForegroundColor = Config.SelectedColor;
                Console.Write("[X]");
            }
            else
            {
                Console.ForegroundColor = Config.NotSelectedColor;
                Console.Write("[ ]");
            }

            Console.BackgroundColor = bgColor;
            Console.ForegroundColor = fgColor;
        }

        public SwitchView SetText(string text)
        {
            this.text = text;
            return this;
        }
    }
}