﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersTest.Views
{
    public class ButtonView : View, IClickable
    {
        private string text = string.Empty;
        private Action onClick;

        public ButtonView(ConsoleConfig config) : base(0, 0, config)
        {
        }

        public void Click()
        {
            onClick?.Invoke();
        }

        public ButtonView Click(Action action)
        {
            onClick = action;
            return this;
        }

        public override void Render(bool selected = false)
        {
            var bgColor = Console.BackgroundColor;
            var fgColor = Console.ForegroundColor;

            if (selected)
            {
                Console.BackgroundColor = Config.BackgroundColor;
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.Write(this.text);
            Console.Write('\t');

            Console.BackgroundColor = bgColor;
            Console.ForegroundColor = fgColor;
        }

        public ButtonView Text(string text)
        {
            this.text = text;
            return this;
        }
    }
}
