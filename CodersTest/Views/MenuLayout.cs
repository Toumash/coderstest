﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersTest.Views
{
    public class MenuLayout
    {
        private List<View> items;
        private int x;
        private int y;

        private bool _escaped;
        public bool Escaped
        {
            get { return _escaped; }
        }

        private string title;
        private ConsoleColor titleForeground;
        private ConsoleColor titleBackground;

        public MenuLayout(int x, int y, string title = "",
            ConsoleColor titleForeground = ConsoleColor.White, ConsoleColor titleBackground = ConsoleColor.Black)
        {
            this.x = x;
            this.y = y;
            this.title = title;
            this.titleForeground = titleForeground;
            this.titleBackground = titleBackground;
        }

        public bool ShowMenu()
        {
            Console.Clear();

            var selection = 0;
            RenderMenu(x, y, selection);
            while (ShowMenu(items, ref selection, Console.ReadKey(intercept: true).Key)) ;
            return _escaped;
        }

        private bool ShowMenu(List<View> items, ref int selection, ConsoleKey key)
        {
            if (key == ConsoleKey.Escape)
            {
                _escaped = true;
                return false;
            }

            var item = items[selection];
            if (key == ConsoleKey.Enter || key == ConsoleKey.Spacebar)
            {
                (item as IClickable)?.Click();
                _escaped = true;
                return false;
            }

            if (key == ConsoleKey.UpArrow && selection - 1 >= 0)
                selection--;

            if (key == ConsoleKey.DownArrow && selection + 1 < items.Count)
                selection++;

            RenderMenu(x, y, selection);

            return true;
        }

        private void RenderMenu(int x, int y, int selection)
        {
            Console.Clear();
            ColorConsole.WriteLine(title, titleForeground, titleBackground);

            for (int i = 0; i < items.Count; i++)
            {
                var toggleView = items[i];
                if (i == selection)
                    toggleView.Render(selected: true);
                else
                    toggleView.Render();

                Console.WriteLine();
            }
        }

        public void SetItems(List<View> items)
        {
            this.items = items;
        }
    }
}
