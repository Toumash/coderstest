﻿namespace CodersTest.Views
{
    public abstract class View
    {
        public int X { get; set; }
        public int Y { get; set; }
        protected ConsoleConfig Config;

        public abstract void Render(bool selected = false);

        protected View(int x, int y, ConsoleConfig config)
        {
            X = x;
            Y = y;
            Config = config;
        }
    }
}
