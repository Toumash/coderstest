﻿namespace CodersTest.Views
{
    public interface IClickable
    {
        void Click();
    }
}
