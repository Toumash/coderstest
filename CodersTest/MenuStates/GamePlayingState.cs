﻿using System;
using System.Diagnostics;
using System.Threading;

namespace CodersTest
{
    public class GamePlayingState : GameState
    {
        private char choosenChar;

        private readonly char[] chars = new char[] { 'q', 'w', 'e', 'r', 't',
            'y', 'u', 'i', 'o', 'p','a','s','d','f','g','h','j','k','l','z',
            'x','c','v','b','n','m' };

        public GamePlayingState(GameContext context) : base(context)
        {
        }

        public override void GoNext()
        {
            Context.State = new MainMenuState(Context);
        }

        public override void OnExitState()
        {
            Console.CancelKeyPress -= HandleControlCInput;
        }

        private void Show(char c)
        {
            if (c == choosenChar)
                ColorConsole.Write(c.ToString(), ConsoleColor.Green);
            else if (c == '\n')
                Console.WriteLine();
            else
                ColorConsole.Write(c.ToString(), ConsoleColor.Red);
        }

        public override void OnEnterState()
        {
            Console.CancelKeyPress += HandleControlCInput;
            Console.Clear();
            Console.Title = "Playing...";
            var swTotal = Stopwatch.StartNew();
            var swCurrent = Stopwatch.StartNew();
            var rand = new Random();
            choosenChar = chars[rand.Next(0, chars.Length)];

            while (true)
            {
                Console.SetCursorPosition(0, 0);
                Console.WriteLine($"Game in progress {swTotal.ElapsedMilliseconds:f2}");

                const string displayChars = "q w e r t y u i o p\n a s d f g h j k l\n  z x c v b n m";
                for (int i = 0; i < displayChars.Length; i++)
                {
                    Show(displayChars[i]);
                }

                Console.Write(string.Join(" ", new char[Console.BufferWidth - Console.CursorLeft]));

                if (Console.KeyAvailable)
                {
                    var keyInfo = Console.ReadKey(intercept: true);
                    var key = keyInfo.Key;
                    var c = keyInfo.KeyChar;
                    if (key == ConsoleKey.Escape)
                    {
                        break;
                    }
                    else if (c >= 'a' && c <= 'z')
                    {
                        if (c == choosenChar)
                        {
                            swCurrent.Restart();
                            choosenChar = chars[rand.Next(0, chars.Length)];
                        }
                    }
                }
                Thread.Sleep(32);
            }
        }

        private static void HandleControlCInput(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
        }
    }
}