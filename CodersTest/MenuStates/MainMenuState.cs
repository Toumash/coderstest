﻿using CodersTest.Menus;
using CodersTest.Views;
using System.Collections.Generic;

namespace CodersTest
{
    public class MainMenuState : GameState
    {
        public GameState _nextState;
        public MainMenuState(GameContext context) : base(context)
        {
        }

        public override void GoNext()
        {
            Context.State = _nextState;
        }

        public override void OnEnterState()
        {
            var views = new List<View>
            {
                new ButtonView(Context.Config)
                .Text("Start Game")
                .Click(() => _nextState = new GamePlayingState(Context)),
                new ButtonView(Context.Config)
                .Text("Settings")
                .Click(() => _nextState = new SettingsMenuState(Context)),
            };

            var layout = new MenuLayout(0, 0, "Welcom to the Coder test, prepare...");
            layout.SetItems(views);
            layout.ShowMenu();
            _nextState = _nextState ?? new MainMenuState(Context);
        }
    }
}
