﻿using CodersTest.Views;
using System;
using System.Collections.Generic;

namespace CodersTest.Menus
{
    public class SettingsMenuState : GameState
    {
        private readonly List<SwitchView> settings = new List<SwitchView>();

        public SettingsMenuState(GameContext context) : base(context)
        {
            settings.Add(new SwitchView(context.Config).SetText("Kolorowy text"));
            settings.Add(new SwitchView(context.Config).SetText("Trolo text"));
        }

        public override void GoNext()
        {
            this.Context.State = new MainMenuState(Context);
        }

        public override void OnEnterState()
        {
            Console.Clear();

            var selection = 0;
            RenderMenu(selection);
            while (ShowMenu(ref selection, Console.ReadKey(intercept: true).Key)) ;
        }

        public bool ShowMenu(ref int selection, ConsoleKey key)
        {
            if (key == ConsoleKey.Escape)
                return false;

            if (key == ConsoleKey.Enter || key == ConsoleKey.Spacebar)
                settings[selection].Click();

            if (key == ConsoleKey.UpArrow && selection - 1 >= 0)
                selection--;

            if (key == ConsoleKey.DownArrow && selection + 1 < settings.Count)
                selection++;

            RenderMenu(selection);

            return true;
        }

        private void RenderMenu(int selection)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Settings Menu. Press ESC to go BACK");
            for (int i = 0; i < settings.Count; i++)
            {
                var toggleView = settings[i];
                if (i == selection)
                    toggleView.Render(selected: true);
                else
                    toggleView.Render();

                Console.WriteLine();
            }
        }
    }
}
