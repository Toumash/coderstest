﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodersTest
{
    using Color = ConsoleColor;
    public class ConsoleConfig
    {
        public ConsoleColor BackgroundColor { get; set; }
        public ConsoleColor SelectedColor { get; set; }
        public ConsoleColor NotSelectedColor { get; set; }

        public static readonly ConsoleConfig Default = new DefaultConsoleConfig();

        class DefaultConsoleConfig : ConsoleConfig
        {
            public DefaultConsoleConfig()
            {
                BackgroundColor = Color.DarkCyan;
                SelectedColor = Color.Green;
                NotSelectedColor = Color.DarkRed;
            }
        }
    }
}
