﻿using System;

namespace CodersTest
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            var game = new GameContext();
            game.Start();
            while (true)
            {
                game.GoNext();
            }
        }
    }
}
