﻿namespace CodersTest
{
    public class GameContext
    {
        public GameState State
        {
            get { return _state; }
            set
            {
                State?.OnExitState();
                _state = value;
                State.OnEnterState();
            }
        }

        private GameState _state;

        public ConsoleConfig Config = ConsoleConfig.Default;

        public void Start()
        {
            State = new MainMenuState(this);
        }

        public void GoNext()
        {
            State.GoNext();
        }
    }
}
