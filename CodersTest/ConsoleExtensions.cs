﻿using System;

namespace CodersTest
{
    using Color = ConsoleColor;
    public static class ColorConsole
    {
        public static void Write(string text, Color foreground = Color.Gray,
            Color background = Color.Black, params object[] args)
        {
            var fgColor = Console.ForegroundColor;
            var bgColor = Console.BackgroundColor;
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(text, args);
            Console.ForegroundColor = fgColor;
            Console.BackgroundColor = bgColor;
        }

        public static void WriteLine(string text, Color foreground = Color.Gray,
            Color background = Color.Black, params object[] args)
        {
            Write(text + "\n", foreground, background, args);
        }
    }
}
